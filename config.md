
The extension will try and search for the specific variable inside the document for configuration or will load them from the default config file **config.txt** (next to this one)

* config variable name - variable name which will contain the the configuration data

* possible arguments:

  * tabRowPosition - possible options are: left, completeLeft right, completeRight, center
  * tabRowLogoCSS - css
  * tabRowLogo - logo filename (in img/logo folder)
  * tabRowLogoPos - logo position - left or right


* example of the variable content

````
tabRowPosition: center
tabRowLogo: MINI_LOGO_SMALL2X.png
tabRowLogoCSS: width: 40px; height: 40px; padding-right: 20px
tabRowLogoPos: left
````