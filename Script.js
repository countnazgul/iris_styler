var cPath = '';

Qva.AddDocumentExtension('FixedCenterContent_MINIQOR', function () {
	var cApath = Qva.Remote + "?public=only&type=Document&name=Extensions/FixedCenterContent_MINIQOR/";
	cPath = cApath;
	Qva.LoadCSS(cApath + "css\\style.css")
	Qva.LoadCSS(cApath + "css\\style_cleanMenu.css")
	Qva.LoadCSS(cApath + "css\\Master.css");

	//qv = this;

	this.Document.SetTabrowPaint(function () {

		//Set reference to original this so we can reference back to orginal scope
		var _this = this;
		$(_this.Element).empty();
		//Remove standard class name and assign our own. This breaks the relationship
		//to standard QV CSS rules
		_this.Element.className = 'Tabrow';

		//Create markup where we later will insert our tab LI elements
		$(_this.Element).append('<div id="navDiv" class="navbar" style="visibility:hidden"></div>');
		$('#navDiv').append('<ul id="root" class="navbar" ></ul>')


		//If tabrow is enabled
		if (_this.Layout.visible) {

			//For each tab...
			$.each(_this.Layout.value, function () {

				//Append li to ul. Can't use jQuery because of custom properties.
				var node = document.createElement("li");
				document.getElementById('root').appendChild(node);
				node.style.display = !this.visible ? "none" : ""; //show tab if visable.
				//node.className = this.selected === "true" ? "active" : ""; //If active tab set className active
				node.id = this.name

				//Append A element to corresponding LI element
				var a = document.createElement("a");
				document.getElementById(this.name).appendChild(a);
				a.innerHTML = this.text
				//Set correct Action which is a custom QV property. This handles onclick events to make sure correct sheet is activated when you click it.
				a.onclick = onclick_action;
				a.Action = _this.Name + "." + this.name

			});
		} else {
			//If tab is disabled or not visable dont paint it.
			_this.Element.style.display = "none";
		};
		//bba()

	});

	this.Document.SetOnUpdateComplete(centerIt);

	var varExists = false;

	this.Document.GetAllVariables(function (vars) {

		for (var i = 0; i < vars.length; i++) {
			var qvVariable = vars[i];
			if (qvVariable.name == 'vIrisConfig') {
				varExists = true;
				var t = qvVariable.value.split("\n");

				for (var a = 0; a < t.length; a++) {
					var index = t[a].indexOf(':');
					var name = t[a].substr(0, index).trim();
					var value = t[a].substr(index + 1, t[a].length - index).trim();

					config[name] = value;
				}
			}
		}

		if (varExists == false) {
			config = {
				"tabRowPosition": "center",
				"tabRowLogo": "MINI_LOGO_SMALL2X.png",
				"tabRowLogoCSS": "width: 40px; height: 40px; padding-right: 20px",
				"tabRowLogoPos": "left"
			}

		}

		centerIt();

	});
});

var config = {};

$(window).resize(function () {
	centerIt();
});


function SetFixed() {

	$('#QvAjaxToolbar').css("position", "fixed");
	$("#QvAjaxToolbar").css("top", "45");
	$("#QvAjaxToolbar").css('z-index', 300);
	$(".Qv_SessionCollaboration").css('z-index', 500); // share session dropdown
	$(".ctx-menu").css('z-index', 501); // right click properties

	$("#DS").css("z-index", "55555"); // Fix for select box list appearing below other content #DaveClarke

	tabrowDisplay = $('#Tabrow').css('display');

	var container = $('#PageContainer')
	var outStep = container[0].getBoundingClientRect().left;


	$("*[class*=_fixed]").each(function () {
		$(this).css("margin-left", outStep + 'px');
		$(this).css("position", "fixed");
		zIndex = $(this).css('z-index');
		zIndex = parseInt(zIndex);
		$(this).css('z-index', zIndex + 100);

		if (tabrowDisplay !== 'none') {
			$(this).css("margin-top", "45px");
		} else {
			$(this).css("margin-top", "45px");
		}

	});

	if (tabrowDisplay !== 'none') {
		$("#Tabrow").css("position", "fixed");
		$("#Tabrow").css("top", "45px");
		$("#Tabrow").css('z-index', 201);
		$("#MainContainer").css("top", "45px");
		$("#MainContainer").css("margin-top", "45px");
	}

	var pageContainer = $('#PageContainer').width();
	var totalW = 0;
	var l = '';

	try {
		if (config.tabRowLogo.length > 0) {
			switch (config.tabRowLogoPos) {
				case 'left':
					l = $('#root li').first();

					if (l[0].innerHTML.slice(0, 4) != '<img') {
						$('#root li:eq(0)').before('<li><img src="' + cPath + "logos\\" + config.tabRowLogo + '" style="' + config.tabRowLogoCSS + '"> </img>  </li>');
					}
					break;
				case 'right':
					l = $('#root li').last();

					if (l[0].innerHTML.slice(0, 4) != '<img') {
						$('#root').append('<li><img src="' + cPath + "logos\\" + config.tabRowLogo + '" style="' + config.tabRowLogoCSS + '"> </img>  </li>');
					}
					break;
			}
		}

		$("#root li").each(function (idx, li) {
			totalW += $(li).width()
		});


		var additionalStep = -1;

		switch (config.tabRowPosition) {
			case 'left':
				additionalStep = 0;
				break;
			case 'completeLeft':
				additionalStep = 0;
				outStep = 0;
				break;
			case 'center':
				additionalStep = (pageContainer - totalW) / 2;
				break;
			case 'right':
				additionalStep = (pageContainer - totalW);
				break;
			case 'completeRight':
				additionalStep = 0;
				outStep = $(window).width() - totalW;
				break;
		}

		$('#navDiv').css("margin-left", (outStep + additionalStep) + 'px');
		$('#navDiv').css("visibility", "visible");
	} catch (ex) {
		console.log(ex)
	}

}

function centerIt() {
	if (!($("body").hasClass("CenterAlign1340"))) {
		$("body").addClass("CenterAlign1340");
		//wrap a container around the whole document and center it.
		$("body").append('<div class="master" />').find('.master').append($('#PageContainer'));
		$("#MainContainer").css("position", "relative");
		//center the tabs if they exist
		$("head").append("<style>.qvtr-tabs{margin:0 auto !important;}</style>");
		//center the background image if there is one.
		$("body").css("background-position", "center 30px");
	}
	var maxRight = 1340;
	//loop through all QV alements on the page and determine the maximum right position on the page
	//in order to determine the bounding box of the QV doc.  It needs to be done this way because all of the elements
	//are absolutely positioned
	$(".QvFrame").each(function () {
		var tMR = $(this).position().left + $(this).width();
		if (tMR > maxRight) {
			maxRight = 1340;
		}
	});
	$(".CenterAlign1340 .master").css("width", maxRight + "px");
	$(".qvtr-tabs").css("width", $(".master").width() + "px");

	SetFixed();
}


